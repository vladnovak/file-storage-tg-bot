package pl.novak.controller;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.novak.service.FileService;

@RestController
@RequiredArgsConstructor
@Slf4j
@RequestMapping("/files")
public class FileController {
    private final FileService fileService;

    @GetMapping("/documents/{id}")
    public ResponseEntity<?> getDocumentById(@PathVariable("id") String id) {
        var doc = fileService.getDocument(id);
        var binaryContent = doc.getBinaryContent();
        var fileSystemResource = fileService.getFileSystemResource(binaryContent);
        if (fileSystemResource == null) {
            log.error("fileSystemResource is null");
            return ResponseEntity.badRequest().build();
        }
        return ResponseEntity.ok()
                .contentType(MediaType.parseMediaType(doc.getMimeType()))
                .header("Content-disposition", "attachment; filename=%s".formatted(doc.getDocumentName()))
                .body(fileSystemResource);
    }

    @GetMapping("/photos/{id}")
    public ResponseEntity<?> getPhotoById(@PathVariable("id") String id) {
        var photo = fileService.getPhoto(id);
        var binaryContent = photo.getBinaryContent();
        var fileSystemResource = fileService.getFileSystemResource(binaryContent);
        if (fileSystemResource == null) {
            log.error("fileSystemResource is null");
            return ResponseEntity.badRequest().build();
        }
        return ResponseEntity.ok()
                .contentType(MediaType.IMAGE_JPEG)
                .header("Content-disposition", "attachment;")
                .body(fileSystemResource);
    }

}
