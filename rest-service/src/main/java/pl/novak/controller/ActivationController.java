package pl.novak.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pl.novak.service.UserActivationService;

@RestController
@RequestMapping("/user")
@RequiredArgsConstructor
public class ActivationController {
    private final UserActivationService userActivationService;

    @GetMapping("/activation/{id}")
    public ResponseEntity<?> activation(@PathVariable("id") String id) {
        var activationResult = userActivationService.activation(id);
        if (activationResult) {
            return ResponseEntity.ok("User account has been successfully activated!");
        } else return ResponseEntity.internalServerError().build();
    }
}
