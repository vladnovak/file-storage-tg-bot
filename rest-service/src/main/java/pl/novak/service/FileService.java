package pl.novak.service;

import org.springframework.core.io.FileSystemResource;
import pl.novak.entity.AppDocument;
import pl.novak.entity.AppPhoto;
import pl.novak.entity.BinaryContent;

public interface FileService {
    AppDocument getDocument(String docId);
    AppPhoto getPhoto(String photoId);
    FileSystemResource getFileSystemResource(BinaryContent binaryContent);
}
