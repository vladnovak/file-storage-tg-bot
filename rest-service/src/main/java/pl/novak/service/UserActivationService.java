package pl.novak.service;

public interface UserActivationService {
    boolean activation(String hashedUserId);
}
