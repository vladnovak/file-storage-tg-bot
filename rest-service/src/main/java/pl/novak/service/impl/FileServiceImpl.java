package pl.novak.service.impl;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;
import org.springframework.core.io.FileSystemResource;
import org.springframework.stereotype.Service;
import pl.novak.entity.AppDocument;
import pl.novak.entity.AppPhoto;
import pl.novak.entity.BinaryContent;
import pl.novak.exception.FileNotFoundException;
import pl.novak.repository.AppDocumentRepository;
import pl.novak.repository.AppPhotoRepository;
import pl.novak.service.FileService;
import pl.novak.utils.CryptoTool;

import java.io.File;
import java.io.IOException;

@Service
@Slf4j
@RequiredArgsConstructor
public class FileServiceImpl implements FileService {
    private final AppDocumentRepository appDocumentRepository;
    private final AppPhotoRepository appPhotoRepository;
    private final CryptoTool cryptoTool;

    @Override
    public AppDocument getDocument(String hash) {
        var id = cryptoTool.idOf(hash);
        return appDocumentRepository
                .findById(id)
                .orElseThrow(() -> new FileNotFoundException("File was not found in the system"));
    }

    @Override
    public AppPhoto getPhoto(String hash) {
        var id = cryptoTool.idOf(hash);
        return appPhotoRepository
                .findById(id)
                .orElseThrow(() -> new FileNotFoundException("File was not found in the system"));
    }

    @Override
    public FileSystemResource getFileSystemResource(BinaryContent binaryContent) {
        try {
            //todo add generation of temp file names
            File temp = File.createTempFile("tempFile", ".bin");
            temp.deleteOnExit();
            FileUtils.writeByteArrayToFile(temp, binaryContent.getFileAsByteArray());
            return new FileSystemResource(temp);
        } catch (IOException ex) {
            log.error(ex.getMessage());
            return null;
        }
    }
}
