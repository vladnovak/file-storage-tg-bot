package pl.novak.service.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import pl.novak.entity.enums.UserState;
import pl.novak.repository.AppUserRepository;
import pl.novak.service.UserActivationService;
import pl.novak.utils.CryptoTool;

@Service
@RequiredArgsConstructor
public class UserActivationServiceImpl implements UserActivationService {
    private final AppUserRepository appUserRepository;
    private final CryptoTool cryptoTool;

    @Override
    public boolean activation(String hashedUserId) {
        var userId = cryptoTool.idOf(hashedUserId);
        var userOptional = appUserRepository.findById(userId);
        if (userOptional.isPresent()) {
            var user = userOptional.get();
            user.setIsActive(true);
            user.setState(UserState.BASIC_STATE);
            appUserRepository.save(user);
            return true;
        } else return false;
    }
}
