package pl.novak.service;


import pl.novak.utils.dto.MailParams;

public interface MailSenderService {
    void send(MailParams mailParams);
}
