package pl.novak.service.impl;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import pl.novak.entity.AppUser;
import pl.novak.repository.AppUserRepository;
import pl.novak.service.AppUserService;
import pl.novak.utils.CryptoTool;
import pl.novak.utils.dto.MailParams;

import static pl.novak.entity.enums.UserState.*;

@Service
@RequiredArgsConstructor
@Slf4j
public class AppUserServiceImpl implements AppUserService {
    private final AppUserRepository appUserRepository;
    private final CryptoTool cryptoTool;
    @Value("${service.mail.uri}")
    private String mailServiceUri;

    @Override
    public String registerUser(AppUser appUser) {
        if (appUser.getIsActive()) {
            return "You are activated already!";
        } else if (appUser.getEmail() != null) {
            return "The activation email has been sent to your email. Please finish the registration by following the link in the email";
        }
        appUser.setState(REGISTRATION_STATE);
        appUserRepository.save(appUser);
        return "Please provide your email address: ";
    }

    @Override
    public String setEmail(AppUser appUser, String email) {
        var appUserOptional = appUserRepository.findByEmail(email);
        if (appUserOptional.isEmpty()) {
            appUser.setEmail(email);
            appUserRepository.save(appUser);

            var hashedUserId = cryptoTool.hashOf(appUser.getId());
            var response = sendRequestToMailService(hashedUserId, email);

            if (response.getStatusCode() != HttpStatus.OK) {
                var message = "Email was not send to %s".formatted(email);
                log.error(message);
                appUser.setEmail(null);
                appUserRepository.save(appUser);
                return message;
            } else
                return "The activation email has been sent to your email. Please follow the activation link in it to complete the registration";
        } else return "This email address is registered already. You can cancel this action by /cancel command.";
    }


    private ResponseEntity<String> sendRequestToMailService(String hashedUserId, String email) {
        var restTemplate = new RestTemplate();
        var headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        var mailParams = MailParams.builder()
                .id(hashedUserId)
                .emailTo(email)
                .build();
        var request = new HttpEntity<>(mailParams, headers);
        return restTemplate.exchange(mailServiceUri,
                HttpMethod.POST,
                request,
                String.class);
    }
}
