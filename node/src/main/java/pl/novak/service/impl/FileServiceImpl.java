package pl.novak.service.impl;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.telegram.telegrambots.meta.api.objects.Document;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.PhotoSize;
import pl.novak.entity.AppDocument;
import pl.novak.entity.AppPhoto;
import pl.novak.entity.BinaryContent;
import pl.novak.exception.UploadFileException;
import pl.novak.repository.AppDocumentRepository;
import pl.novak.repository.AppPhotoRepository;
import pl.novak.repository.BinaryContentRepository;
import pl.novak.service.FileService;
import org.json.JSONObject;
import pl.novak.service.enums.LinkType;
import pl.novak.utils.CryptoTool;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;

@Service
@Slf4j
@RequiredArgsConstructor
public class FileServiceImpl implements FileService {
    @Value("${telegram.token}")
    private String token;
    @Value("${service.file_info.uri}")
    private String fileInfoUri;
    @Value("${service.file_storage.uri}")
    private String fileStorageUri;
    @Value("${link.host}")
    private String linkHost;
    private final AppDocumentRepository appDocumentRepository;
    private final AppPhotoRepository appPhotoRepository;
    private final BinaryContentRepository binaryContentRepository;
    private final CryptoTool cryptoTool;

    @Override
    public AppDocument processDocument(Message telegramMessage) {
        var fileId = telegramMessage.getDocument().getFileId();
        ResponseEntity<String> response = getFilePath(fileId);
        if (response.getStatusCode().is2xxSuccessful()) {
            var persistedBinaryContent = getPersistedBinaryContent(response);
            var telegramDocument = telegramMessage.getDocument();
            AppDocument transientDocument = buildTransientAppDoc(telegramDocument, persistedBinaryContent);
            return appDocumentRepository.save(transientDocument);
        } else throw new UploadFileException("Bad response from Telegram: %s".formatted(response));
    }

    private BinaryContent getPersistedBinaryContent(ResponseEntity<String> response) {
        var filePath = getFilePath(response);
        byte[] fileInByte = downloadFile(filePath);
        var binaryContent = BinaryContent.builder()
                .fileAsByteArray(fileInByte)
                .build();
        var persistedBinaryContent = binaryContentRepository.save(binaryContent);
        return persistedBinaryContent;
    }

    private static String getFilePath(ResponseEntity<String> response) {
        JSONObject jsonObject = new JSONObject(response.getBody());
        var filePath = String.valueOf(jsonObject
                .getJSONObject("result")
                .getString("file_path"));
        return filePath;
    }

    @Override
    public AppPhoto processPhoto(Message telegramMessage) {
        //todo now only one photo is being processed
        var photoSizeCount = telegramMessage.getPhoto().size();
        var photoIndex = photoSizeCount > 1 ? photoSizeCount - 1 : 0;
        var telegramPhoto = telegramMessage.getPhoto().get(photoIndex);
        var photoId = telegramPhoto.getFileId();
        var response = getFilePath(photoId);
        if (response.getStatusCode().is2xxSuccessful()) {
            var persistedBinaryPhoto = getPersistedBinaryContent(response);
            var transientAppPhoto = buidTransientAppPhoto(telegramPhoto, persistedBinaryPhoto);
            return appPhotoRepository.save(transientAppPhoto);
        }
        return null;
    }

    @Override
    public String generateLink(Long docId, LinkType linkType) {
        var hashedId = cryptoTool.hashOf(docId);
        var linkWithId = linkType.toString().replace("{id}", hashedId);
        return linkHost + linkWithId;
    }

    private AppDocument buildTransientAppDoc(Document telegramDocument, BinaryContent binaryContent) {
        return AppDocument.builder()
                .binaryContent(binaryContent)
                .documentName(telegramDocument.getFileName())
                .fileSize(telegramDocument.getFileSize())
                .telegramFileId(telegramDocument.getFileId())
                .mimeType(telegramDocument.getMimeType())
                .build();
    }

    private AppPhoto buidTransientAppPhoto(PhotoSize photo, BinaryContent binaryContent) {
        return AppPhoto.builder()
                .binaryContent(binaryContent)
                .fileSize(photo.getFileSize())
                .telegramFileId(photo.getFileId())
                .build();
    }

    private byte[] downloadFile(String filePath) {
        var fullUri = fileStorageUri.replace("{token}", token)
                .replace("{filePath}", filePath);
        URL urlObject = null;
        try {
            urlObject = new URL(fullUri);
        } catch (MalformedURLException ex) {
            throw new UploadFileException("Exception during URL creation in FileServiceImpl.downloadFile(String filePath)");
        }

        //todo optimize this part; big files should be downloaded as chunks
        try (InputStream is = urlObject.openStream()) {
            return is.readAllBytes();
        } catch (IOException e) {
            throw new UploadFileException("Exception during reading bytes form IS in FileServiceImpl.downloadFile");
        }
    }

    private ResponseEntity<String> getFilePath(String fileId) {
        var restTemplate = new RestTemplate();
        var headers = new HttpHeaders();
        HttpEntity<String> request = new HttpEntity<>(headers);

        return restTemplate.exchange(
                fileInfoUri,
                HttpMethod.GET,
                request,
                String.class,
                token, fileId
        );
    }
}
