package pl.novak.service.impl;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.validator.routines.EmailValidator;
import org.springframework.stereotype.Service;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Update;
import pl.novak.entity.AppUser;
import pl.novak.entity.RawData;
import pl.novak.exception.UploadFileException;
import pl.novak.repository.AppUserRepository;
import pl.novak.repository.RawDataRepository;
import pl.novak.service.AppUserService;
import pl.novak.service.FileService;
import pl.novak.service.MainService;
import pl.novak.service.ProducerService;
import pl.novak.service.enums.LinkType;
import pl.novak.service.enums.ServiceCommands;

import static pl.novak.entity.enums.UserState.*;

@Service
@Slf4j
@RequiredArgsConstructor
public class MainServiceImpl implements MainService {
    private final RawDataRepository rawDataRepository;
    private final ProducerService producerService;
    private final AppUserRepository appUserRepository;
    private final FileService fileService;
    private final AppUserService appUserService;
    private final EmailValidator emailValidator;

    @Override
    public void processTextMessage(Update update) {
        saveRawData(update);

        var appUser = findOrSaveAppUser(update);
        var text = update.getMessage().getText();

        var output = "";

        var textIsEmail = emailValidator.isValid(text);
        var serviceCommand = ServiceCommands.fromValue(text);

        if (textIsEmail) {
            output = processEmailInput(text, appUser);
        } else if (serviceCommand != null) {
            output = processServiceCommand(serviceCommand, appUser);
        } else {
            output = processUnknownCommand(text);
        }

        var chatId = update.getMessage().getChatId();
        sendAnswer(output, chatId);
    }

    private String processUnknownCommand(String input) {
        return "Unknown command: %s.\n%s".formatted(input, help());
    }

    private String processRequestFromUnregisteredUser() {
        return "In order to use the service please register your email with /registration command.";
    }

    private String processEmailInput(String input, AppUser appUser) {
        return switch (appUser.getState()) {
            case UNREGISTERED_STATE -> processRequestFromUnregisteredUser();
            case REGISTRATION_STATE -> appUserService.setEmail(appUser, input);
            case BASIC_STATE ->
                    appUser.getEmail().equals(input) && appUser.getIsActive() ? "Your email is already confirmed" : processUnknownCommand(input);
        };
    }

    private String processServiceCommand(ServiceCommands serviceCommand, AppUser appUser) {
        return switch (serviceCommand) {
            case CANCEL -> cancelProcess(appUser);
            case HELP -> help();
            case START -> "Hi! Type /help to see all available commands.";
            case REGISTRATION -> appUserService.registerUser(appUser);
        };
    }

    @Override
    public void processPhotoMessage(Update update) {
        saveRawData(update);
        var appUser = findOrSaveAppUser(update);
        var chatId = update.getMessage().getChatId();
        if (isNotAllowedToSendContent(chatId, appUser)) {
            return;
        }
        try {
            var photo = fileService.processPhoto(update.getMessage());
            var link = fileService.generateLink(photo.getId(), LinkType.GET_PHOTO);
            var answer = "Your photo has been stored in the database and can be downloaded at %s".formatted(link);
            sendAnswer(answer, chatId);

        } catch (UploadFileException ex) {
            log.error(ex.getMessage());
            sendAnswer("File upload failed, please retry...", chatId);
        }

    }

    @Override
    public void processDocumentMessage(Update update) {
        saveRawData(update);
        var appUser = findOrSaveAppUser(update);
        var chatId = update.getMessage().getChatId();
        if (isNotAllowedToSendContent(chatId, appUser)) {
            return;
        }

        try {
            var doc = fileService.processDocument(update.getMessage());
            var link = fileService.generateLink(doc.getId(), LinkType.GET_DOC);
            var answer = "Your document has been stored in the database and can be downloaded at %s".formatted(link);
            sendAnswer(answer, chatId);
        } catch (UploadFileException ex) {
            log.error(String.valueOf(ex));
            sendAnswer("File upload failed, please retry...", chatId);
        }


    }

    private boolean isNotAllowedToSendContent(Long chatId, AppUser appUser) {
        var userState = appUser.getState();
        if (!appUser.getIsActive()) {
            var error = "Please complete process of registration to be able to upload content.";
            sendAnswer(error, chatId);
            //todo add correct implementation
            return true;
        } else if (!userState.equals(BASIC_STATE)) {
            var error = "You do not have BASIC_STATE, please cancel the current command with /cancel command";
            sendAnswer(error, chatId);
            //todo add correct implementation
            return true;
        }
        return false;
    }


    private String help() {
        return """
                List of all available commands:
                /cancel - cancels current command;
                /registration - starts the registration of a new user;
                """;
    }

    private String cancelProcess(AppUser appUser) {
        if (appUser.getState().equals(REGISTRATION_STATE)) {
            appUser.setState(UNREGISTERED_STATE);
        }
        appUserRepository.save(appUser);
        return "Command has been cancelled!";
    }

    private void sendAnswer(String output, Long chatId) {
        var answer = new SendMessage();
        answer.setChatId(chatId);
        answer.setText(output);
        producerService.producerAnswer(answer);
    }

    private void saveRawData(Update update) {
        var rawData = RawData.builder()
                .event(update)
                .build();
        rawDataRepository.save(rawData);
    }

    private AppUser findOrSaveAppUser(Update update) {
        var telegramUser = update.getMessage().getFrom();
        return appUserRepository
                .findByTelegramUserId(telegramUser.getId())
                .orElseGet(() -> appUserRepository.save(
                        AppUser.builder()
                                .telegramUserId(telegramUser.getId())
                                .userName(telegramUser.getUserName())
                                .firstName(telegramUser.getFirstName())
                                .lastName(telegramUser.getLastName())
                                .isActive(false)
                                .state(UNREGISTERED_STATE)
                                .build()
                ));
    }
}
