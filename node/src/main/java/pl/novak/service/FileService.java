package pl.novak.service;

import org.telegram.telegrambots.meta.api.objects.Message;
import pl.novak.entity.AppDocument;
import pl.novak.entity.AppPhoto;
import pl.novak.service.enums.LinkType;

public interface FileService {
    AppDocument processDocument(Message telegramMessage);
    AppPhoto processPhoto(Message telegramMessage);
    String generateLink(Long docId, LinkType linkType);

}
