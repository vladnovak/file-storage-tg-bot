package pl.novak.service.enums;

public enum LinkType {
    GET_DOC("/files/documents/{id}"),
    GET_PHOTO("/files/photos/{id}");
    private final String link;
    LinkType(String link) {
        this.link = link;
    }

    @Override
    public String toString() {
        return link;
    }
}
