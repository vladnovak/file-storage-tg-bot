package pl.novak.service.enums;

public enum ServiceCommands {
    START("/start"),
    HELP("/help"),
    REGISTRATION("/registration"),
    CANCEL("/cancel");
    private final String cmd;

    ServiceCommands(String cmd) {
        this.cmd = cmd;
    }

    @Override
    public String toString() {
        return cmd;
    }

    public boolean equals(String cmd) {
        return this.toString().equals(cmd);
    }

    public static ServiceCommands fromValue(String value) {
        for (ServiceCommands command : ServiceCommands.values()) {
            if (command.equals(value)) {
                return command;
            }
        }
        return null;
    }
}
