package pl.novak.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import pl.novak.utils.CryptoTool;

@Configuration
public class NodeConfig {
    @Value("${salt}")
    private String salt;

    @Bean
    public CryptoTool configureCryptoTool() {
        return new CryptoTool(salt);
    }
}
