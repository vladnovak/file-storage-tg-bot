package pl.novak.config;

import org.apache.commons.validator.routines.EmailValidator;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

@Component
public class EmailValidatorConfig {
    @Bean
    public EmailValidator configureEmailValidator() {
        return EmailValidator.getInstance();
    }
}
