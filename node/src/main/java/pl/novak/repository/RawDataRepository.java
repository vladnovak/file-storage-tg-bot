package pl.novak.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.novak.entity.RawData;

public interface RawDataRepository extends JpaRepository<RawData, Long> {
}
