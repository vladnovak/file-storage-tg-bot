package pl.novak.config;

import org.springframework.amqp.core.Queue;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


@Configuration
public class RabbitConfig {
    @Value("${spring.rabbitmq.queues.text-message-update}")
    private String textQueueName;
    @Value("${spring.rabbitmq.queues.photo-message-update}")
    private String photoQueueName;
    @Value("${spring.rabbitmq.queues.doc-message-update}")
    private String documentQueueName;
    @Value("${spring.rabbitmq.queues.answer-message}")
    private String answerQueue;
    @Bean
    public MessageConverter jsonMessageConverter() {
        return new Jackson2JsonMessageConverter();
    }

    @Bean
    public Queue textMessageQueue() {
        return new Queue(textQueueName);
    }

    @Bean
    public Queue photoMessageQueue() {
        return new Queue(photoQueueName);
    }

    @Bean
    public Queue docMessageQueue() {
        return new Queue(documentQueueName);
    }

    @Bean
    public Queue answerMessageQueue() {
        return new Queue(answerQueue);
    }
}
