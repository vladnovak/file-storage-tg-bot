package pl.novak.config;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.telegram.telegrambots.meta.TelegramBotsApi;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;
import org.telegram.telegrambots.updatesreceivers.DefaultBotSession;
import pl.novak.controller.TelegramBot;
import pl.novak.controller.UpdateController;

@Configuration
@RequiredArgsConstructor
public class TelegramConfig {
    @Value("${telegram.token}")
    private String token;
    @Value("${telegram.bot_name}")
    private String botName;
    private final UpdateController updateController;

    @Bean
    public TelegramBot telegramBot() {
        return new TelegramBot(token, botName, updateController);
    }
    @Bean
    public TelegramBotsApi botsApi(TelegramBot telegramBot) throws TelegramApiException {
        TelegramBotsApi botsApi = new TelegramBotsApi(DefaultBotSession.class);
        botsApi.registerBot(telegramBot);
        return botsApi;
    }
}
