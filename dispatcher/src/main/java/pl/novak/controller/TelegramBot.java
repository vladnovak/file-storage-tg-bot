package pl.novak.controller;

import jakarta.annotation.PostConstruct;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

@Slf4j
@AllArgsConstructor
public class TelegramBot extends TelegramLongPollingBot {
    private final String botName;
    private UpdateController updateController;

    @PostConstruct
    public void init() {
        updateController.registerTelegramBot(this);
    }

    public TelegramBot(String token, String botName, UpdateController updateController) {
        super(token);
        this.botName = botName;
        this.updateController = updateController;
    }

    @Override
    public void onUpdateReceived(Update update) {
        updateController.processUpdate(update);
    }

    @Override
    public String getBotUsername() {
        return botName;
    }

    public void sendAnswerMessage(SendMessage message) {
        if (message != null) {
            try {
                execute(message);
            } catch (TelegramApiException ex) {
                log.error(ex.getMessage());
            }
        }
    }
}
