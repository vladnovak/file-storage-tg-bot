( rabbitmqctl wait --timeout 60 $RABBITMQ_PID_FILE ; \
rabbitmqctl add_user $FSBOT_RABBITMQ_USER $FSBOT_RABBITMQ_PASSWORD 2>/dev/null ; \
rabbitmqctl set_user_tags $FSBOT_RABBITMQ_USER administrator ; \
rabbitmqctl set_permissions -p / $FSBOT_RABBITMQ_USER  '.*' '.*' '.*' ; \
echo "*** User '$FSBOT_RABBITMQ_USER' with password '$FSBOT_RABBITMQ_PASSWORD' completed. ***" ; \
echo "*** Log in the WebUI at port 15672 (example: http:/localhost:15672) ***") &
rabbitmq-server $@