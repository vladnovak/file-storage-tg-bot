package pl.novak.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.novak.entity.BinaryContent;

public interface BinaryContentRepository extends JpaRepository<BinaryContent, Long> {
}
