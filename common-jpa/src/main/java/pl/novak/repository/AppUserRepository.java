package pl.novak.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.novak.entity.AppUser;

import java.util.Optional;

public interface AppUserRepository extends JpaRepository<AppUser, Long> {
    Optional<AppUser> findByTelegramUserId(Long telegramUserId);
    Optional<AppUser> findById(Long id);
    Optional<AppUser> findByEmail(String email);
}
