package pl.novak.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.novak.entity.AppDocument;

import java.util.Optional;

public interface AppDocumentRepository extends JpaRepository<AppDocument, Long> {
    //Optional<AppDocument> findAppDocumentById(Long id);
}
