package pl.novak.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.novak.entity.AppPhoto;

public interface AppPhotoRepository extends JpaRepository<AppPhoto, Long> {
}
