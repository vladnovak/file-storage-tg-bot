package pl.novak.entity.enums;

public enum UserState {
    BASIC_STATE,
    UNREGISTERED_STATE,
    REGISTRATION_STATE
}
