package pl.novak.utils.dto;

import lombok.*;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter
@Setter
public class MailParams {
    private String id;
    private String emailTo;
}
