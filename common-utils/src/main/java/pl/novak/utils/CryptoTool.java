package pl.novak.utils;

import org.hashids.Hashids;

public class CryptoTool {
    private final Hashids hashids;

    public CryptoTool(String salt) {
        var minHashLength = 10;
        this.hashids = new Hashids(salt, minHashLength);
    }

    public String hashOf(Long id) {
        return hashids.encode(id);
    }

    public Long idOf(String hash) {
        long[] decodedResult = hashids.decode(hash);
        if (decodedResult != null && decodedResult.length > 0) {
            return decodedResult[0];
        }
        return null;
    }
}
